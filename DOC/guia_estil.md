# Guía d'estil Cine Grup 2

Partim d'una base de guia de CSS estàndard *normalise*, per deixar el css a 0. A partir d'aquí creem el nostre.

###Header
=============
####Color :  
Lila rgb(119, 109, 109);
####Tipografia :  
[https://fonts.googleapis.com/css?family=Montserrat][Montserrat] 
Com a font principal a tots els Headers, part **esquerra** **superior**, amb **font-size: 4em**, i a la part dreta **font-size: 3em**.

A la resta de site s'utilitza  utilitzem aquestes tipografies i mides

**font-size: 1em;**
**font-family: Arial, Helvetica, sans-serif;**

I amb la plantilla d' skeleton:
**font-size: 1.5em;
font-family: "Raleway", "HelveticaNeue", "Helvetica Neue", Helvetica, Arial, sans-serif;
h1, h2, h3, h4, h5, h6: 2rem**

A la primera pàgina i última tenim un botó de login i logout pel administrador. Aquest anirà situat a **top: 40%; right: 3%;** amb fons de color **white**

Els elements generals van situats de forma central.

###Footer:###
Logo del cinema situat a  **2% right left**, i amb un **width** del **15%**, amb els color stàndard del cine.

###Imatges pel·lícules:###
Les imateges utilitzades per les sessions aniran amb una mida del **25%**, situades al **30% top bottom**,  **margin-left:15** i **box-shadow: 7px 6px 11px 0px rgba(0, 0, 0, 0.68)**.

###Buttons:###
Fons blanc, lletra color gris, stàndard mil·ligram. Podrà variar la seva amplada depenén les necessitats.

###Formulari:###
Formulari, situat a la part central, respectant la norma bàsica, les mides han de ser:  **height: 100px** **width: 70% margin: 10% 10% 10% 15%**, amb el **color taronja** del cinema de fons, i **box-shadow: 7px 6px 11px 0px rgba(0, 0, 0, 0.68);**