DROP TABLE IF EXISTS entrada;
DROP TABLE IF EXISTS sesion;
DROP TABLE IF EXISTS usuario;
DROP TABLE IF EXISTS pelicula;

/*ENGINE=InnoDB DEFAULT CHARSET=latin1;*/

CREATE TABLE pelicula (
    id_pelicula tinyint UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	nombre varchar(255) NOT NULL,
	sinopsis text NOT NULL,
    duracion char(3) NOT NULL,
	imagen varchar(255) NOT NULL
    );

CREATE TABLE usuario (
    email varchar(255) PRIMARY KEY,
	nombre varchar(255) NOT NULL,
	apellido varchar(255) NOT NULL,
	telefono char(9) NOT NULL
);

CREATE TABLE sesion (
	fecha date PRIMARY KEY,
    hora time,
    dia_espectador boolean,
    tiene_vip boolean,
    id_pelicula tinyint UNSIGNED AUTO_INCREMENT,
    FOREIGN KEY (id_pelicula) REFERENCES pelicula (id_pelicula)
   );

CREATE TABLE entrada (
	fecha date,
    butaca char(3),
    precio tinyint,
    email varchar(255),
    PRIMARY KEY (fecha, butaca),
    FOREIGN KEY (email) REFERENCES usuario (email),
    FOREIGN KEY (fecha) REFERENCES sesion (fecha)
);


INSERT INTO pelicula (nombre, sinopsis, duracion, imagen) VALUES
('Titanic', 'Jack (DiCaprio), un joven artista, gana en una partida de cartas un pasaje para viajar a América en el Titanic, el transatlántico más grande y seguro jamás construido. A bordo conoce a Rose (Kate Winslet), una joven de una buena familia venida a menos que va a contraer un matrimonio de conveniencia con Cal (Billy Zane), un millonario engreído a quien sólo interesa el prestigioso apellido de su prometida. Jack y Rose se enamoran, pero el prometido y la madre de ella ponen todo tipo de trabas a su relación. Mientras, el gigantesco y lujoso transatlántico se aproxima hacia un inmenso iceberg.', '179', 'titanic.jpg'),
('El club de la lucha', 'Un joven hastiado de su gris y monótona vida lucha contra el insomnio. En un viaje en avión conoce a un carismático vendedor de jabón que sostiene una teoría muy particular: el perfeccionismo es cosa de gentes débiles; sólo la autodestrucción hace que la ', '139', 'el_club_de_la_lucha.jpg'),
('Mortal Engines', 'Miles de años después de la destrucción de la civilización por un cataclismo, la humanidad se ha adaptado y, ahora, existen gigantescas ciudades en movimiento que vagan por la tierra sobre enormes ruedas absorbiendo a los pueblos más pequeños para obtener', '128', 'mortal_enginyes.jpg'),
('Aquaman', 'Cuando Arthur Curry (Jason Momoa) descubre que es mitad humano y mitad atlante, emprenderá el viaje de su vida en esta aventura no sólo le obligará a enfrentarse a quién es en realidad, sino también a descubrir si es digno de cumplir con su destino: ser r', '139', 'aquaman.jpg'),
('Trote', 'Hija de hermana de Carme vive en una aldea de las montañas del interior de Galicia junto a su padre Ramón, con el que apenas se comunica, y su madre enferma. Trabaja en una panadería y necesita escapar de ese ambiente opresivo, pero las circunstancias sie', '83', 'trote.jpg'),
('Stubby, un héroe muy especial', 'El joven militar Robert Conroy ve cómo su vida cambia cuando encuentra a un pequeño perro en el campamento donde entrena para la guerra. A pesar de no tener formación militar, el perro se embarca en la aventura, donde luchará junto a Robert y las tropas f', '85', 'stubby.jpg'),
('La família', 'Andrés y su hijo Pedro, de 12 años, viven en una vecindad marginal de Caracas y apenas se ven. Mientras Andrés utiliza su tiempo con sus trabajos, Pedro camina por las calles jugando con sus amigos y aprendiendo del lugar violento que le rodea. ', '82', 'la_familia.jpg'),
('Expediente 64', 'La historia arranca con un descubrimiento: tras una pared falsa, se hallan tres cadáveres momificados alrededor de una mesa, y junto a un asiento libre. El detective Carl Mørck y su asistente Assad seguirán las pistas hasta una institución donde tenían lu', '100', 'journal_64.jpg'),
('Galveston', 'Después de escapar de una emboscada, un sicario enfermo vuelve a su ciudad natal de Galveston donde planea su venganza. Roy Cady es un asesino a sueldo en New Orleans al que le fue diagnosticado cáncer de pulmón a los 40 años. Ante la sospecha de que su j', '94', 'galveston.jpg'),
('Kursk', 'Narra la tragedia del submarino nuclear ruso 2000 K-141 Kursk ocurrida en agosto del año 2000, y la negligencia gubernamental que le siguió. Mientras los marineros luchan por sobrevivir, sus familias luchan desesperadamente contra los obstáculos políticos', '117', 'kursk.jpg');

INSERT INTO usuario VALUES('1@hotmail.com','Juan','Garcia','931283456');

INSERT INTO sesion VALUES('2018-12-12', 160000, true, true, 1);
INSERT INTO sesion VALUES('2018-12-20', 200000, false, true, 1);
INSERT INTO sesion VALUES('2018-12-21', 160000, false, false, 1);
INSERT INTO sesion VALUES('2018-12-22', 200000, true, true, 2);
INSERT INTO sesion VALUES('2018-12-23', 200000, false, false, 3);
INSERT INTO sesion VALUES('2018-12-25', 200000, false, true, 4);
INSERT INTO sesion VALUES('2018-12-30', 160000, true, false, 5);

INSERT INTO entrada VALUES('2018-12-30','A1',6,'1@hotmail.com');
