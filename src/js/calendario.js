var revealingModule = (function() {

function esBisiesto(ano) {
    return ((ano % 4 == 0) && (ano % 100 != 0)) || (ano % 400 == 0);
}

function cantidadDeDias(ano, mes) {
    switch (mes) {
        case 3:
        case 5:
        case 8:
        case 10:
            return 30;
        case 0:
        case 2:
        case 4:
        case 6:
        case 7:
        case 9:
        case 11:
            return 31;
        case 1:
            if (esBisiesto(ano)) return 29;
            else return 28;
    }
}

function creaCalendario (ano, mes, dia) {

    let diaSemanaDelPrimero = new Date(ano, mes, 1).getDay();
    let diasEnEsteMes = cantidadDeDias(ano, mes);
    let diasMesAnterior;
    let dias = new Array(42);
    let i;
    let j;
    let k=0;
    let calendarioHTML = '';
    let nombreMes = ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
    let hoy = `${ano}-${mes+1}-${dia}`;
    let diaRecoriendose;
    let clickable;
    let x = document.cookie;
    let diasConSesion = x.substring(x.indexOf("=")+1).split("+");
    
    if (mes == 0) {
        diasMesAnterior = cantidadDeDias(ano - 1, 11);
    } else {
        diasMesAnterior = cantidadDeDias(ano, mes - 1);
    }

    /*La posición 0 es para el domingo, lo coloco en la posicion 7 y los días de la semana serán del 1 al 7*/
    if (diaSemanaDelPrimero == 0) {
        diaSemanaDelPrimero = 7;
    }

    for (i = 0, j = diasMesAnterior - diaSemanaDelPrimero + 2; i < diaSemanaDelPrimero-1; i++ , j++) dias[i] = j;
    for (i, j = 0; j < diasEnEsteMes; i++ , j++) dias[i] = j + 1;
    for (i, j = 1; i < 42; i++, j++) dias[i] = j;

    calendarioHTML +=   `<h3>${nombreMes[mes]}</h3>
                            <table>
                                <tr>
                                    <th>Lun</th>
                                    <th>Mar</th>
                                    <th>Mie</th>
                                    <th>Jue</th>
                                    <th>Vie</th>
                                    <th>Sab</th>
                                    <th>Dom</th>
                                </tr>`;
    mes++;
    for (i=0; i<6; i++) {
        calendarioHTML += "<tr>";
        for (j=0; j<7; j++, k++) {
            if ((i==0) && (dias[k]>7)) {
                clase = "mesAnterior";
                diaRecoriendose = `${ano}-${mes-1}-${dias[k]}`;
            }
            else if ((i>3) && (dias[k]<15)) {
                clase = "mesSiguiente";
                diaRecoriendose = `${ano}-${mes+1}-${dias[k]}`;
            } else {
                clase = "esteMes";
                diaRecoriendose = `${ano}-${mes}-${dias[k]}`;
                if (hoy==diaRecoriendose) clase += " hoy";
                if (diasConSesion.indexOf(diaRecoriendose)!=-1) clickable = true;
            }

            if (clickable) calendarioHTML += `<td class='${clase} sesion'><a href="JavaScript:void(0)">${dias[k]}</a></td>`;
            else calendarioHTML += `<td class='${clase}'>${dias[k]}</td>`;
            clase="";
            clickable = false;
        }
        calendarioHTML += "</tr>";
    }

    calendarioHTML +=   `</table>
                    </div>`;
    
    return calendarioHTML;
}

return {
    
    pcalendarioHTML: creaCalendario,
    pcantidadDeDias: cantidadDeDias,
  
}  
})();

$(document).ready(function () {
    
    let fecha = new Date();
    let dia = fecha.getDate();
    let mes = fecha.getMonth();
    let ano = fecha.getFullYear();

    let calendario = revealingModule.pcalendarioHTML(ano, mes, dia);
    
    $("#calendario").html(calendario);

    $('#proxMes').click(function () {
        if (mes == 11) {
          ano = ano + 1;
          mes = 0;
        } else {
          mes = mes + 1;
        }
        calendario = revealingModule.pcalendarioHTML(ano, mes, 0);
        $("#calendario").html(calendario);
    });
    
    $('#antMes').click(function () {
        if (mes == 0) {
          ano = ano - 1;
          mes = 11;
        } else {
          mes = mes - 1;
        }
        calendario = revealingModule.pcalendarioHTML(ano, mes, 0);
        $("#calendario").html(calendario);
    });

    $(".sesion").click(function () {
        if (!$(this).hasClass("elegido")) {
            $("td").removeClass("elegido");
            $(this).addClass("elegido");
        }
        let diaElegido = $(this).text();
        diaElegido = `${ano}-${mes+1}-${diaElegido}`;
        $(".fecha").val(diaElegido);
    });
});