<?php 

session_start();
if (!isset($_SESSION['email'])) {
    header ("Location: ../index.php");
}

include_once '../private/includes/db.php';
include_once '../private/includes/funciones.php';

foreach ($_SESSION as $clave => $valor) $$clave = $_SESSION[$clave];

if ($dia_espectador) $stringEspectador = "- <b class='true'>Día del espectador</b>";
else $stringEspectador = "";

$string =   "<img class='imagen_pelicula' src='../img/peliculas/$imagen'>
                <div class='info_pelicula'>
                <h3>$nombre_pelicula</h3>
                <p>Sinopsis: $sinopsis<p>
                <small>Duración: $duracion min</small>
                <p>Hora: $hora $stringEspectador</p></div>";

//Butacas
$butacas = butacas();
$ocupadas = butacasOcupadas($conn, $fecha);
$claseButaca="";
$tabla="<table>";
for ($i=0, $k=0; $i<12; $i++) {

    $tabla.= "<tr>";

    for ($j=0; $j<10; $j++, $k++) {

        $claseButaca ="butaca";

        if (in_array($butacas[$k], $ocupadas)) $claseButaca .= " ocupada";
        else $claseButaca .= " disponible";
        if (($tiene_vip==1) && ($i==5)) $claseButaca .= " vip";

        $tabla.= "<td id='$butacas[$k]' class='$claseButaca'></td>";
    }

    $tabla.= "</tr>";
}
$tabla.="</table>";

?>

<!-- PAGINA -->

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cinema</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/reset.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../css/micss.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../css/skeleton.css" />
    <link href="../lib/montserrat.css" rel="stylesheet">
    <script src="../lib/jquery-3.3.1.min.js"></script>
    <script src="../js/butacas.js"></script>
    <script src="../js/pestanas.js"></script>
</head>

<body>
    <header>    
        <div class="nom_cine">Cines IAM</div>
        <div class="cabecera">butacas</div>
    </header>
    <ul id="lista">
        <li class="active">Películas</li>
        <li>Butacas</li>        
    </ul>
    

    <div class="module">
        <?php echo $string;?>
    </div>

    <!-- butacas -->
    <div class="module butacass">
        <?php echo $tabla; ?>
        <form action="../private/includes/butacas-inc.php" method="POST">
            <input type="hidden" name="butacas" id="butacas">
            <strong>Precio: <span></span>€</strong>
            <input class="button_comprar" type="submit" name="submit" id="siguiente" value="Seleccionar">
        </form>
    </div>
    <div>
        <img class="logo_nom_cine" src="../img/IAM_CAT_logos_transparent_rgb_1.png">
    </div>
</body>
</html>