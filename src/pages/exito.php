<?php 

session_start();
if (!isset($_SESSION['butacas'])) {
    header ("Location: ../index.php");
}
session_destroy();

?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Éxito</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/reset.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../css/micss.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../css/skeleton.css" />
    <link href="../lib/montserrat.css" rel="stylesheet">
    <script src="../lib/jquery-3.3.1.min.js"></script>
</head>

<body>

    <header>
        <div class="nom_cine">Cines IAM</div>
        <button class="admin" type="button" name="logout">Logout</button>
    </header>
    <div class="padre_cont_body">
        <div class="flotante"></div>
        <div class="cont_body">
            <section>
                <img class="image_error" src="../img/exito.png">
            </section>
        </div>
    </div>
    <div class="exito">
        <h5>¡Compra finalizada! </h5>
        </form>
    </div>
    <div>
        <img class="logo_nom_cine" src="../img/IAM_CAT_logos_transparent_rgb_1.png">
    </div>
</body>

</html>
