<?php
session_start();
if (!isset($_SESSION['butacas'])) {
    header ("Location: ../index.php");
}
?>

<!DOCTYPE html>
<html>

<head>
<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cinema</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/reset.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../css/micss.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../css/skeleton.css" />
    <link href="../lib/montserrat.css" rel="stylesheet">
    <script src="../lib/jquery-3.3.1.min.js"></script>
</head>

<body>

    <header>
        <div class="nom_cine">Cines IAM</div>
        <div class="cabecera">datos personales</div>
    </header>

<div class="marco">

<div class="form_datos">
    <h3 style="text-align:center">Introduce tus datos</h3>
    <form action="../private/includes/datos-inc.php" method="POST">
        <div class="row">
            <div class="six columns">
                <label for="nombre"></label>
                <input class="u-full-width" type="text" placeholder="Nombre" id="nombre" name="nombre">
                <label for="apellido"></label>
                <input class="u-full-width" type="text" placeholder="Apellido" id="apellido" name="apellido">
                <label for="telefono"></label>
                <input class="u-full-width" type="text" placeholder="Teléfono" id="telefono" name="telefono">
                <input style="background:white"name="submit" type="submit" value="Checkout">
            </div>
        </div>
    </form>
</div>
</div>
    <div>
        <img class="logo_nom_cine" src="../img/IAM_CAT_logos_transparent_rgb_1.png">
    </div>
</body>

</html>