<?php
session_start();

if (!isset($_SESSION['email'])) {
    header ("Location: ../index.php");
}

foreach ($_SESSION as $clave => $valor) $$clave = $_SESSION[$clave];
$string = "Hola $nombre, ya has comprado una entrada para nuestra sesión el día $fecha. Estas son tus butacas: <b>$butacas</b>";
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Éxito</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../css/reset.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../css/micss.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../css/skeleton.css" />
    <link href="../lib/montserrat.css" rel="stylesheet">
    <script src="../lib/jquery-3.3.1.min.js"></script>
</head>

<body>

    <header>
        <div class="nom_cine">Cines IAM</div>
        <div class="cabecera">error</div>
    </header>
    <div class="padre_cont_body">
        <div class="flotante"></div>
        <div class="cont_body">
            <section>
                <img class="image_error image_error_compradas" src="../img/error.png">
            </section>
        </div>
    </div>
    <div class="text_error text_error_compradas ">
        <hr>
        <h5>No puedes comprar dos veces</h5>
        <p><?php echo $string;?></p>
        
    </div>
    <div>
        <img class="logo_nom_cine" src="../img/IAM_CAT_logos_transparent_rgb_1.png">
    </div>
</body>

</html>

<?php session_destroy(); ?>