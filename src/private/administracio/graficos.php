<?php
include_once '../includes/funciones.php';
include_once '../includes/db.php';
$_POST = testPost($_POST);
$fecha=$_POST['fecha'];
$butacasOcupadas=count(butacasOcupadas($conn, $fecha));
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cinema</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="../../css/reset.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../../css/micss.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="../../css/skeleton.css" />
    <link href="../../lib/montserrat.css" rel="stylesheet">
    <script src="../../lib/jquery-3.3.1.min.js"></script>
    <script src="../../lib/jsapi.js"></script>

    <script>
        google.load("visualization", "1", {packages:["corechart"]});
        google.setOnLoadCallback(dibujarGrafico);
   
        function dibujarGrafico() {
        // Tabla de datos: valores y etiquetas de la gráfica
            var data = google.visualization.arrayToDataTable([
                ['Butaca','Cantidad'],
                ['Butacas vendidas',<?php echo $butacasOcupadas;?>],
                ['Butacas libres',<?php echo 120 - $butacasOcupadas;?>]
        ]);
        
            var options = {
                title: 'INFORMACION BUTACAS',
                slices: {
                    0: { color: '#ff9900' },
                    1: { color: '#7844c5' }
                },
                is3D: true
            };
        
            // Dibujar el gráfico
            new google.visualization.PieChart(document.getElementById('chart')).draw(data, options);
    }
 </script> 
</head>

<body>
                <div id="chart" ></div>

</body>
</html>
