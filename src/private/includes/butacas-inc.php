<?php

include_once 'funciones.php';

if (isset($_POST['submit'])) {
    //codigo que comprueba que no se ha modificado el hidden input para introducir código malicioso
    $butacasElegidas = test_input($_POST['butacas']);
    $butacasValidas = butacas();
    $butacasElegidas = explode(",", $butacasElegidas);
    for ($i=0; $i<count($butacasElegidas); $i++) {
        if (!in_array($butacasElegidas[$i], $butacasValidas)) {
            die("Butaca elegida incorrecta");
        }
    }
    session_start();
    $_SESSION['butacas'] = $_POST['butacas'];    
    header ("Location: ../../pages/datos.php");
}

?>