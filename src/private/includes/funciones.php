<?php

//queries

function querySQL($conn, $query) {
    $sql= mysqli_query($conn, $query);
    if ($sql==FALSE) die ("Fallo: " . mysqli_error($conn));
    else return $sql;
}

function estaRegistrado ($conn, $email) {
    $query = "SELECT * FROM usuario WHERE email='$email'";
    $sql= querySQL($conn, $query);
    $rows = mysqli_num_rows($sql);
    if ($rows>0) return true;
    else return false;
}

function tieneSesionesFuturas ($conn, $email) {
    $hoy = date("Y-m-d");
    $query =    "SELECT e.fecha 
                FROM usuario u 
                JOIN entrada e
                ON u.email=e.email 
                WHERE u.email='$email'
                AND e.fecha>='$hoy'
                LIMIT 1";
    $sql= querySQL($conn, $query);
    $rows = mysqli_num_rows($sql);
    if ($rows>0) return true;
    return false;
}

function datosDelUsuario($conn, $email) {
    $query =    "SELECT u.nombre, e.fecha, GROUP_CONCAT(e.butaca SEPARATOR ', ') AS butacas
                FROM usuario u
                JOIN entrada e
                ON u.email=e.email
                WHERE u.email='$email'";
    $sql= querySQL($conn, $query);
    $consulta = mysqli_fetch_assoc($sql);
    return $consulta;
}

function datosSesion($conn, $fecha) {
    $query =    "SELECT p.nombre AS 'nombre_pelicula', p.sinopsis, p.duracion, p.imagen, s.tiene_vip, s.hora, s.dia_espectador 
                FROM pelicula p 
                JOIN sesion s 
                ON p.id_pelicula=s.id_pelicula
                WHERE fecha='$fecha'";
    $sql= querySQL($conn, $query);
    $consulta = mysqli_fetch_assoc($sql);
    return $consulta;
}

function butacasOcupadas ($conn, $fecha) {
    $query= "SELECT e.butaca 
            FROM entrada e 
            JOIN sesion s 
            ON e.fecha = s.fecha
            WHERE s.fecha='$fecha'";
    $sql= mysqli_query($conn, $query);
    $rows = mysqli_num_rows($sql);
    $ocupadas = [];
        for ($i = 0 ; $i < $rows; $i++) {
            $consulta = mysqli_fetch_assoc($sql);
            foreach ($consulta as $clave => $valor) {
                array_push($ocupadas, $valor);
            }
        }
    return $ocupadas;
}

function sesionesEnCookie ($conn) {
    $query= "SELECT fecha FROM sesion";
    $sql= mysqli_query($conn, $query);
    $rows = mysqli_num_rows($sql);
    $diasConSesion = "";
    for ($i = 0 ; $i < $rows; $i++) {
        $consulta = mysqli_fetch_assoc($sql);
        foreach ($consulta as $valor) {
            $diasConSesion .= "$valor ";
        }
    }
setcookie("diasConSesion", $diasConSesion);
}

//inputs

function test_input($string) {
    $string = trim($string);
    $string = stripslashes($string);
    $string = htmlspecialchars($string);
    return $string;
}

function testPost ($post) {
    array_pop($post);
    foreach ($post as $clave => $valor) $post[$clave] = test_input($post[$clave]);
    return $post;
}

function camposCompletados ($post) {
    foreach ($post as $clave => $valor) if ($post[$clave]==NULL) return false;
    return true;
}

//fechas

function validaFecha($fecha) {
    $fechaArray = explode('-', $fecha);
    for ($i=0; $i<count($fechaArray); $i++) {
        $fechaArray[$i]=intval($fechaArray[$i]);
        if (!is_int($fechaArray[$i])) {
            return false;
        }
    }

    return checkdate($fechaArray[1], $fechaArray[2], $fechaArray[0]);
}

function diaYaPaso($fecha, $hoy) {

    $fechaArray = explode('-', $fecha);
    $hoyArray = explode('-', $hoy);

    if ($fechaArray[0]-$hoyArray[0]<0) {
        return true;
    } else {
        if ($fechaArray[1]-$hoyArray[1]<0) {
            return true;
        } else {
            if ($fechaArray[2]-$hoyArray[2]<0) {
                return true;
            }
        }
    }

    return false;

}

//otros

function butacas () {
    $butacas=[];
    foreach (range('A', 'L') as $fila) {
        $idButaca = $fila;
        for ($i=0; $i<10; $i++) {
            $idButaca .= $i+1;
            array_push($butacas, $idButaca);
            $idButaca=$fila;
    
        }
    }
    return $butacas;
}

function convertirCookieSesion ($string) {

    $sesionesDisponibles = explode(" ", $string);
    array_pop($sesionesDisponibles);
    return $sesionesDisponibles;
}


?>