<?php

include_once 'funciones.php';
include_once 'db.php';

if(isset($_POST['submit'])) {
    
    $_POST=testPost($_POST);
    
    if (camposCompletados($_POST)) {

        if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {

        //crea $email y $fecha para no usar un array asociativo
        foreach ($_POST as $clave => $valor) $$clave = $_POST[$clave];
        $hoy = date("Y-m-d");
        $diasConSesion = convertirCookieSesion($_COOKIE['diasConSesion']);

        $_SESSION = Array();
        session_start();
        $_SESSION['email']=$email;
        $_SESSION['fecha']=$fecha;

            if (validaFecha($fecha)) {

                if (in_array($fecha, $diasConSesion)) {

                    if (!diaYaPaso($fecha, $hoy)) {

                        //datos de sesion
                        $datosSesion = datosSesion($conn, $fecha);
                        //indexa en sesión nombre de película, sinopsis, duracion, imagen, si tiene vip y si es el dia de espectador
                        foreach ($datosSesion as $clave => $valor) $_SESSION[$clave] = $datosSesion[$clave];

                        if (estaRegistrado($conn, $email)) {
                            //necesito esta variable para saber si el usuario quiere modificar sus datos
                            $_SESSION['registrado'] = true;

                            if (tieneSesionesFuturas($conn, $email)) {
                                
                                array_pop($_SESSION);
                                $datosUsuario=datosDelUsuario($conn, $email);
                                //indexa en sesión fecha, nombre, fecha de sesión comprada y butacas
                                foreach ($datosUsuario as $clave => $valor) $_SESSION[$clave] = $datosUsuario[$clave];                             
                                /*sobreescribimos el valor de $_SESSION[fecha] con la fecha para 
                                la que ha comprado sesión anteriormente porque el día que eligió 
                                se vuelve irrelevante ya que será reenviado al resumen de su compra*/

                                header ("Location: ../../pages/resumen.php");

                            } else {
                                
                                header ("Location: ../../pages/butacas.php");

                            }
                            
                        } else {

                            $_SESSION['registrado'] = false;

                            header ("Location: ../../pages/butacas.php");

                        }

                    } else {

                        die("Esa función ya ha pasado");

                    }


                } else {

                    die("Lo sentimos, ese día no habrá función");

                }

            } else {

                die("Fecha incorrecta listillo");

            }

        } else {

            die("Correo incorrecto");
        }

    } else {

        die("Coloca tu correo y elige un día");

    }

} else {

    header ("Location: ../../index.php");

}
?>